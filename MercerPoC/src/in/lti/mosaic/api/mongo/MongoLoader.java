package in.lti.mosaic.api.mongo;

import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSInputFile;

import in.lnt.constants.Constants;
import in.lnt.utility.general.Cache;

/**
 * @author rushikesh
 * @author Vivek K
 *
 */
public class MongoLoader {

	private MongoLoader() {
		throw new IllegalStateException("MongoLoader class");
	}

	private static final Logger logger = LoggerFactory.getLogger(MongoLoader.class);
	
	private static String requestCollectionName = Cache
			.getMLProperty(in.lnt.utility.constants.CacheConstats.MONGO_REQUEST_COLLECTION_NAME);
	private static String responseCollectionName = Cache
			.getMLProperty(in.lnt.utility.constants.CacheConstats.MONGO_RESPONSE_COLLECTION_NAME);
	private static Integer defaultChuncksize = 4096 * 1024;
	
	/**
	 * 
	 * @param mongoObject
	 */
	public static void dumpDataToMongo(MongoObject mongoObject) {
		
		try {
			
			long st = System.currentTimeMillis();
			
			String collectionName = null;
			if (StringUtils.equalsIgnoreCase(mongoObject.getObjectType(), Constants.REQUEST)) {
				collectionName = requestCollectionName;
			} else {
				collectionName = responseCollectionName;
			}
			
			MongoConnector.getMongoDBConnection();

			JSONObject doc = new JSONObject();
			doc.put("request_id", mongoObject.getRequestId() + "");
			doc.put("json", mongoObject.getObjectJson());
			doc.put("timeStamp", new BasicDBObject("date", mongoObject.getTimeStamp()));
			
			//Used this deprecated Method as NON-deprecated method does not support new GridFS(db, collectionName) 
			@SuppressWarnings("deprecation")
			DB db = MongoConnector.mongoClient.getDB(MongoConnector.dbName);
			
			// By GridFS
			GridFS gridfs = new GridFS(db, collectionName);
			GridFSInputFile gfsFile = gridfs
					.createFile(IOUtils.toInputStream(doc.toString(), StandardCharsets.UTF_8.name()));
			gfsFile.setChunkSize(defaultChuncksize);
			gfsFile.setFilename(mongoObject.getRequestId() + "");
			gfsFile.save();

			logger.error("Time taken for dump : Request ID : {}, Time: {}", mongoObject.getRequestId(), (System.currentTimeMillis() - st));
			
		} catch (Exception e) {
			logger.error("Error in dumpDataToMongo : {}" , e.getMessage());
		}
	}

}

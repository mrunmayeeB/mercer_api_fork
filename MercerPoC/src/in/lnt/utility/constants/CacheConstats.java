package in.lnt.utility.constants;

public class CacheConstats {

	public static final String MAXIQ_HOME = "MAXIQ_HOME";
	public static final String API_HOME = "API_HOME";
	
	public static final String MONGO_SERVER_IP = "MONGO_SERVER_IP";
	public static final String MONGO_SERVER_PORT = "MONGO_SERVER_PORT";
	public static final String MONGO_COLLECTION_NAME = "MONGO_COLLECTION_NAME";
	public static final String MERCER_HOME = "MERCER_HOME";

	public static final String RABBITMQ_PRODUCER_IP = "RABBITMQ_PRODUCER_IP";
	public static final String RABBITMQ_PRODUCER_PORT = "RABBITMQ_PRODUCER_PORT";
	public static final String RABBITMQ_PRODUCER_USERID = "RABBITMQ_PRODUCER_USERID";
	public static final String RABBITMQ_PRODUCER_SECRETWORD = "RABBITMQ_PRODUCER_PASSWORD";

	public static final String RABBITMQ_CONSUMER_IP = "RABBITMQ_CONSUMER_IP";
	public static final String RABBITMQ_CONSUMER_PORT = "RABBITMQ_CONSUMER_PORT";
	public static final String RABBITMQ_CONSUMER_USERID = "RABBITMQ_CONSUMER_USERID";
	public static final String RABBITMQ_CONSUMER_SECRETWORD = "RABBITMQ_CONSUMER_PASSWORD";

	public static final String RABBITMQ_PRODUCER_VHOST_NAME = "RABBITMQ_PRODUCER_VHOST_NAME";
	public static final String RABBITMQ_PRODUCER_EXCHANGE_NAME = "RABBITMQ_PRODUCER_EXCHANGE_NAME";
	public static final String RABBITMQ_PRODUCER_BINDING_KEY = "RABBITMQ_PRODUCER_BINDING_KEY";
	public static final String RABBITMQ_PRODUCER_QUEUE_NAME = "RABBITMQ_PRODUCER_QUEUE_NAME";

	public static final String RABBITMQ_CONSUMER_VHOST_NAME = "RABBITMQ_CONSUMER_VHOST_NAME";
	public static final String RABBITMQ_CONSUMER_EXCHANGE_NAME = "RABBITMQ_CONSUMER_EXCHANGE_NAME";
	public static final String RABBITMQ_CONSUMER_BINDING_KEY = "RABBITMQ_CONSUMER_BINDING_KEY";
	public static final String RABBITMQ_CONSUMER_QUEUE_NAME = "RABBITMQ_CONSUMER_QUEUE_NAME";

	public static final String HDFS_USERNAME = "HDFS_USERNAME";
	public static final String HDFS_SECRETWORD = "HDFS_PASSWORD";
	public static final String HDFS_BYPASS_SECRETWORD = "HDFS_BYPASS_PASSWORD";
	public static final String HDFS_STRICT_HOST_KEY_CHECKING = "HDFS_STRICT_HOST_KEY_CHECKING";
	public static final String HDFS_IP = "HDFS_IP";
	public static final String HDFS_STORE_PATH = "HDFS_STORE_PATH";
	public static final String CLUSTER_DESTINATION_INTERIM_PATH = "CLUSTER_DESTINATION_INTERIM_PATH";
	public static final String CLUSTER_DESTINATION_INTERIM_PATH_TODELETE = "CLUSTER_DESTINATION_INTERIM_PATH_TODELETE";

	public static final String MERCER_DB_DRIVER = "MERCER_DB_DRIVER";
	public static final String MERCER_DB_ADDRESS = "MERCER_DB_ADDRESS";
	public static final String MERCER_DB_NAME = "MERCER_DB_NAME";
	public static final String MERCER_DB_USERNAME = "MERCER_DB_USERNAME";
	public static final String MERCER_DB_SECRETWORD = "MERCER_DB_PASSWORD";
	public static final String IS_AUTOCORRECTION = "IS_AUTOCORRECTION";
	public static final String URL_AUTOCORRECTION = "URL_AUTOCORRECTION";

	// ML Constants Starts
	public static final String ML_INPUTLOC = "/home/maxiq/Mercer_ML_Directory/Autocorrect/Data/Incoming/Data";
	public static final String ML_OUTPUTLOC = "/home/maxiq/Mercer_ML_Directory/Autocorrect/Data/Outgoing/Data";

	public static final String ML_REMOTE_ADD = "hn1-Mercer";
	public static final String ML_REMOTE_UNAME = "maxiq";
	public static final String ML_REMOTE_SECRETWORD = "MercerDoc1";

	public static final String ML_REMOTE_SCRIPT = "python -W\"ignore\" /home/maxiq/Mercer_ML_Directory/Autocorrect/Scripts/Autocorrect_v3.py ";
	public static final int ML_RESPONSE_TIMEOUT = 60 * 1000;
	public static final String ML_SUCCESS_RESPONSE = "ML SUCCEED";
	public static final String ML_FAILURE_RESPONSE = "ML FAILED";
	public static final String ML_REMOTE_PORT = "22";
	// ML Constants Ends

	// MongoDB Constants Starts
	public static final String MONGO_DB_NAME = "MONGO_DB_NAME";
	public static final String MONGO_REQUEST_COLLECTION_NAME = "MONGO_REQUEST_COLLECTION_NAME";
	public static final String MONGO_RESPONSE_COLLECTION_NAME = "MONGO_RESPONSE_COLLECTION_NAME";
	public static final String MAX_CAPACITY_OF_MONGO_THREAD = "MAX_CAPACITY_OF_MONGO_THREAD";
	public static final String MONGODUMP_FLAG = "MONGODUMP_FLAG";
	public static final String MONGODB_USERNAME = "MONGODB_USERNAME";
	public static final String MONGODB_SECRETWORD = "MONGODB_PASSWORD";
	// MongoDB Constants Ends

	private CacheConstats() {}
}


package com.lti.mosaic.function.def;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.math3.stat.StatUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lti.mosaic.parser.driver.FunctionDriver;
import com.lti.mosaic.parser.enums.FunctionEnum;
import com.lti.mosaic.parser.utils.MathConstants;


public class AggregateFunctions {
	
	private static final Logger logger = LoggerFactory.getLogger(AggregateFunctions.class);
	
	private Map<String, ArrayList<String[]>> inputDataMaster = new HashMap<>();	// CSC
	private ArrayList<String[]> csvData = new ArrayList<>();
 	FunctionEnum functionName;
 	String expression;
	CharSequence inversionFunctionName1 = FunctionEnum.AGGREGATERANGECHECK.toString();
	CharSequence inversionFunctionName2 = FunctionEnum.INVERSIONCHECK.toString();

	
 	
 	private static final String GROUP_BY_COL = "GROUP_BY_COL";
	private static final String CONDITION_PERFORMED = "CONDITION_PERFORMED";
	private static final String RESULT = "RESULT";
 	
	public AggregateFunctions(Map<String, ArrayList<String[]>> inputDataMaster, FunctionEnum functionName, String expression) {
		this.inputDataMaster = inputDataMaster; 
		this.functionName = functionName;
		this.expression = expression;
	}

	public static final String SELECTINDEX = "selectIndex";
	public static final String CONDITIONINDEX = "conditionIndex";
	public static final String GROUPBYINDEX = "groupByIndex";
	public static final String CONDITIONINDEXOPERATOR = "conditionIndexOperator";
	public static final String GROUPBYINDEXOPERATOR = "groupByIndexOperator";
	public static final String GROUPBYCONDITION = "groupByCondition";
	public static final String SELECTIONCONDITION = "selectionCondition";
	private static final String MAXIQ_DELIMATOR = "";
	private static final String REGEX_FOR_SPLIT_EXPRESSION = "[>=|<=|>|<|!=|=]";

	public Value aggregationDriver(List<Value> args) { 
		String[] parsedSelectCondition = null;
		String[] parsedGroupByCondition = null;
		// CSC
		getCSVData(args.get(0).toString());
		if (checkTypeOfFunction(functionName)) {
			parsedSelectCondition = args.get(2).asString().trim().split(REGEX_FOR_SPLIT_EXPRESSION);
			parsedGroupByCondition =  args.get(3).asString().trim().split(REGEX_FOR_SPLIT_EXPRESSION);
		} else {
			parsedSelectCondition =  args.get(1).asString().trim().split(REGEX_FOR_SPLIT_EXPRESSION);
			parsedGroupByCondition =  args.get(2).asString().trim().split(REGEX_FOR_SPLIT_EXPRESSION);
		}
		// Removing Double quotes if any in the Value
		parsedSelectCondition[parsedSelectCondition.length - 1] = FunctionDriver
				.cleanSingleQuoteStartAndEnd(parsedSelectCondition[parsedSelectCondition.length - 1]);		
		parsedGroupByCondition[parsedGroupByCondition.length - 1] = FunctionDriver
				.cleanSingleQuoteStartAndEnd(parsedGroupByCondition[parsedGroupByCondition.length - 1]);
		String[] arguments = new String[args.size()];
		for (int i = 0; i < args.size(); i++) {
			arguments[i] = args.get(i).toString();
		}
		 
		switch (functionName) {
		case XAVG:
			return getAggregation(arguments, parsedSelectCondition, parsedGroupByCondition, FunctionEnum.XAVG);
		case XCOUNT:
			return getAggregation(arguments, parsedSelectCondition, parsedGroupByCondition, FunctionEnum.XCOUNT);
		case XMAX:
			return getAggregation(arguments, parsedSelectCondition, parsedGroupByCondition, FunctionEnum.XMAX);
		case XMIN:
			return getAggregation(arguments, parsedSelectCondition, parsedGroupByCondition, FunctionEnum.XMIN);
		case XUNIQUECOUNT:
			return getAggregation(arguments, parsedSelectCondition, parsedGroupByCondition, FunctionEnum.XUNIQUECOUNT);
		case XSUM:
			return getAggregation(arguments, parsedSelectCondition, parsedGroupByCondition, FunctionEnum.XSUM);
		case XFREQCOUNT:
			return getAggregation(arguments, parsedSelectCondition, parsedGroupByCondition, FunctionEnum.XFREQCOUNT);
		case XFREQPERC:
			return getAggregation(arguments, parsedSelectCondition, parsedGroupByCondition, FunctionEnum.XFREQPERC);
		case XPERC:
			return getAggregation(arguments, parsedSelectCondition, parsedGroupByCondition, FunctionEnum.XPERC);
		default :
		}
		return null;

	}
	
	// CSC - Get data based on column 
	// PE - 
	private void getCSVData(String param) {
		Iterator<String> iterator = inputDataMaster.keySet().iterator();
		while (iterator.hasNext()) {
			String key = iterator.next();
			ArrayList<String[]> data = inputDataMaster.get(key);
			String[] dataRow = data.get(0);
			for (String dataRowValue : dataRow) {
				if(dataRowValue.equals(param.trim())){
				 csvData = data;
				return;
				}
			}
		}
	}
	
	 Value getAggregation(String[] params, String[] parsedSelectCondition, String[] parsedGroupByCondition,
			FunctionEnum functionName) {
		HashMap<String, JSONObject> result = new HashMap<>();
		Value output = null;
		
		if (!csvData.isEmpty()) {

			// Get columns based on condition given
			JSONObject indicesAndOperations = getIndicesAndOperations(params, parsedSelectCondition,
					parsedGroupByCondition, functionName);

			// Skip Execution if incorrect column names found
			exitIfNotValidExpression(params, indicesAndOperations);

			logger.info("indicesAndOperations  {}", indicesAndOperations);

			// Some Condition Available
			HashMap<String, ArrayList<Object>> intermediateResult = iterateRows(params, parsedSelectCondition,
					parsedGroupByCondition, indicesAndOperations);

			logger.info("intermediateResult {}", intermediateResult);

			// Checking if any key is Null
			HashMap<String, ArrayList<Object>> tempIntermediateResult = new HashMap<>();
			Set<String> keys = intermediateResult.keySet();
			String nullKey = "";
			for (String key : keys) {
				if (null == key) {
					nullKey = key;
					tempIntermediateResult.put("null", intermediateResult.get(key));
				}
			}
			intermediateResult.remove(nullKey);
			intermediateResult.putAll(tempIntermediateResult);

			
			keys = intermediateResult.keySet();
			for (String key : keys) {
				if(iterateResult(params, functionName, result, indicesAndOperations, intermediateResult, key)) {
					continue;
				}
			}

			output = returnResult(functionName, result, keys);

			// If no Condition Present
			return output;
		}else{
			throw new RuntimeException(
					"Invalid aggregate function over selected data Or Invalid input for Expression");
		}}

	/**
	 * @param functionName
	 * @param result
	 * @param output
	 * @param indicesAndOperations
	 * @param keys
	 * @return
	 * @throws RuntimeException
	 */
	private Value returnResult(FunctionEnum functionName, HashMap<String, JSONObject> result, Set<String> keys) {
		Value output;
		if ((result.size() != 0) && !keys.isEmpty()) {
			if (result.size() != 0) {
				output = getOutputBasedOnResult(result);
			} else {
				throw new RuntimeException("No Result for Aggregate!");
			}
		} else {
			// Returning 0
			if ((functionName.compareTo(FunctionEnum.XCOUNT) == 0)
					|| (functionName.compareTo(FunctionEnum.XFREQCOUNT) == 0)
					|| (functionName.compareTo(FunctionEnum.XFREQPERC) == 0)
					|| (functionName.compareTo(FunctionEnum.XPERC) == 0)
					|| (functionName.compareTo(FunctionEnum.XUNIQUECOUNT) == 0)) {
				output = new Value(0);
			} else {
				output = null;
			}
		}
		return output;
	}

	/**
	 * @param result
	 * @return
	 */
	private Value getOutputBasedOnResult(HashMap<String, JSONObject> result) {
		Value output;
		Set<String> resultKeys = result.keySet();
		if ((null != expression) && (!expression.contains(inversionFunctionName1)
				&& !expression.contains(inversionFunctionName2))) {
			output = new Value(result.get(resultKeys.toArray()[0])
					.get(result.get(resultKeys.toArray()[0]).get("SELECT_COL")));
		} else {
			output = new Value(new org.json.JSONObject(result).toString());
		}
		return output;
	}

	/**
	 * @param params
	 * @param functionName
	 * @param result
	 * @param indicesAndOperations
	 * @param intermediateResult
	 * @param key
	 * @return 
	 * @return
	 * @throws RuntimeException
	 */
	private boolean iterateResult(String[] params, FunctionEnum functionName, HashMap<String, JSONObject> result,
			JSONObject indicesAndOperations, HashMap<String, ArrayList<Object>> intermediateResult, String key) {
		
		JSONObject resultObject = new JSONObject();

		// Remove Nulls from List
		// PE - 
		intermediateResult.get(key).removeAll(Collections.singleton(null));
		
		 if(intermediateResult.get(key).isEmpty()){
			 // Continue the loop as no input to Process
         	return true;
         }
		
		// This is used in AGGREGATERANGECHECK & INVERSIONCHECK
		setInfoIntoResultObject(indicesAndOperations, key, resultObject);
		
		resultObject.put("COUNT", intermediateResult.get(key).size());

		if (intermediateResult.get(key).isEmpty()) {
			resultObject.put(csvData.get(0)[(int) indicesAndOperations.get(SELECTINDEX)], resultObject.get(key));
			result.put((String) resultObject.get(GROUP_BY_COL) + MAXIQ_DELIMATOR + (null != key ? key : "null"),
					resultObject);
			return false;
		}

		// Get Type of intermediateResult (Numeric / String) From first 10 records
		
		double[] doubleArray = checkTheTypeOfData(intermediateResult, key);				
		
		String number = null;
		
		try {
			switch (functionName) {
			case XAVG:
				resultObject.put(key, StatUtils.mean(doubleArray));
				break;
			case XCOUNT:
				resultObject.put(key, intermediateResult.get(key).size());
				break;
			case XUNIQUECOUNT:
				resultObject.put(key, new HashSet<Object>(intermediateResult.get(key)).size());
				break;
			case XMAX:
				resultObject.put(key, StatUtils.max(doubleArray));
				break;
			case XMIN:
				resultObject.put(key, StatUtils.min(doubleArray));
				break;
			case XSUM:
				resultObject.put(key, StatUtils.sum(doubleArray));
				break;
			case XFREQCOUNT:
				number = params[1].trim();
				resultObject.put(key, Collections.frequency(intermediateResult.get(key),
						NumberUtils.isNumber(number) ? Double.parseDouble(number) : number));
				break;
			case XFREQPERC:
				number = params[1];
				resultObject.put(key,
						(Double.valueOf(Collections.frequency(intermediateResult.get(key),
								NumberUtils.isNumber(number) ? Double.parseDouble(number) : number)
								/ Double.valueOf(intermediateResult.get(key).size())) * 100));
				break;
			case XPERC:
				number = params[1].trim();
				resultObject.put(key, StatUtils.percentile(doubleArray,
						NumberUtils.isNumber(number) ? Double.parseDouble(number) : 0));
				break;
			default:
				break;
			}
		} catch (Exception e) {
			logger.error("Exception occured {}", e);
			throw new RuntimeException(
					"Invalid aggregate function over selected data Or Invalid input for Expression");

		}
		
		resultObject.put(csvData.get(0)[(int) indicesAndOperations.get(SELECTINDEX)], resultObject.get(key));
		result.put((String) resultObject.get(GROUP_BY_COL) + MAXIQ_DELIMATOR + (null != key ? key : "null"),
				resultObject);
		return false;
	}

	/**
	 * @param indicesAndOperations
	 * @param key
	 * @param resultObject
	 */
	private void setInfoIntoResultObject(JSONObject indicesAndOperations, String key, JSONObject resultObject) {
		if (indicesAndOperations.containsKey(GROUPBYINDEX))
			resultObject.put(GROUP_BY_COL, csvData.get(0)[(int) indicesAndOperations.get(GROUPBYINDEX)]);
		if (indicesAndOperations.containsKey(SELECTINDEX))
			resultObject.put("SELECT_COL", csvData.get(0)[(int) indicesAndOperations.get(SELECTINDEX)]);
		if (indicesAndOperations.containsKey(GROUPBYINDEX))
			resultObject.put(csvData.get(0)[(int) indicesAndOperations.get(GROUPBYINDEX)], key);
	}

	/**
	 * @param intermediateResult
	 * @param key
	 * @param isTypeDouble
	 * @return
	 */
	private double[] checkTheTypeOfData(HashMap<String, ArrayList<Object>> intermediateResult, String key) {
		int isTypeDouble = 0;
		double[] emptyArray = null;
		for (int i = 0; i < (10 < intermediateResult.get(key).size() ? 10
				: intermediateResult.get(key).size()); i++) {
			if (intermediateResult.get(key).get(i) instanceof Double) {
				isTypeDouble++;
			} else {
				isTypeDouble--;
			}
		}
		if (isTypeDouble > 0) {
			return ArrayUtils.toPrimitive(
					intermediateResult.get(key).toArray(new Double[intermediateResult.get(key).size()]));
		}
		return emptyArray;
	}

	/**
	 * @param params
	 * @param parsedSelectCondition
	 * @param parsedGroupByCondition
	 * @param indicesAndOperations
	 * @return
	 */
	private HashMap<String, ArrayList<Object>> iterateRows(String[] params, String[] parsedSelectCondition,
			String[] parsedGroupByCondition, JSONObject indicesAndOperations) {
		HashMap<String, ArrayList<Object>> intermediateResult = new HashMap<>();
		if (indicesAndOperations.containsKey(CONDITIONINDEX)) {
			performConditionBasedEvalutation(params, parsedSelectCondition, parsedGroupByCondition,
					indicesAndOperations, intermediateResult);
		} else if (indicesAndOperations.containsKey(GROUPBYINDEX)) {
			performGroupByBasedEvalutationOnly(params, parsedGroupByCondition, indicesAndOperations,
					intermediateResult);
		} else {
			performPlainAggragtion(params, indicesAndOperations, intermediateResult);
		}
		return intermediateResult;
	}

	/**
	 * @param params
	 * @param indicesAndOperations
	 * @param intermediateResult
	 */
	private void performPlainAggragtion(String[] params, JSONObject indicesAndOperations,
			HashMap<String, ArrayList<Object>> intermediateResult) {
		for (int index = 1; index < csvData.size(); index++) {
			try {
				getSelectionBasedAggregationOnly((int) indicesAndOperations.get(SELECTINDEX), intermediateResult,
						index, params[0], -1);
			} catch (Exception e) {
				continue;
			}
		}
	}

	/**
	 * @param params
	 * @param parsedGroupByCondition
	 * @param indicesAndOperations
	 * @param intermediateResult
	 */
	private void performGroupByBasedEvalutationOnly(String[] params, String[] parsedGroupByCondition,
			JSONObject indicesAndOperations, HashMap<String, ArrayList<Object>> intermediateResult) {
		for (int index = 1; index < csvData.size(); index++) {
			try {
				getGroupByBasedAggregationOnly(indicesAndOperations, intermediateResult, index,
						parsedGroupByCondition, params[0]);
			} catch (Exception e) {
				continue;
			}
		}
	}

	/**
	 * @param params
	 * @param parsedSelectCondition
	 * @param parsedGroupByCondition
	 * @param indicesAndOperations
	 * @param intermediateResult
	 */
	private void performConditionBasedEvalutation(String[] params, String[] parsedSelectCondition,
			String[] parsedGroupByCondition, JSONObject indicesAndOperations,
			HashMap<String, ArrayList<Object>> intermediateResult) {
		for (int index = 1; index < csvData.size(); index++) {
			if (performOperation((String) indicesAndOperations.get(CONDITIONINDEXOPERATOR), index,
					indicesAndOperations, parsedSelectCondition, parsedGroupByCondition, SELECTIONCONDITION)) {
				getConditionBasedAggregation(indicesAndOperations, intermediateResult, index,
						parsedGroupByCondition, params[0]);
			}
		}
	}

	/**
	 * @param params
	 * @param indicesAndOperations
	 * @param validFilterCondition
	 * @param validGroupByCondition
	 * @throws RuntimeException
	 */
	private void exitIfNotValidExpression(String[] params, JSONObject indicesAndOperations) {
		
		//Check Filter Condition
		final boolean validFilterCondition = (indicesAndOperations.containsKey(CONDITIONINDEX)
				&& !indicesAndOperations.containsKey(CONDITIONINDEXOPERATOR))
				|| (indicesAndOperations.containsKey(CONDITIONINDEXOPERATOR)
						&& !indicesAndOperations.containsKey(CONDITIONINDEX));
		
		//Check Group By Condition
		final boolean validGroupByCondition = (indicesAndOperations.containsKey(GROUPBYINDEXOPERATOR)
						&& !indicesAndOperations.containsKey(GROUPBYINDEX));
		
		if ((!indicesAndOperations.containsKey(SELECTINDEX))
				|| validFilterCondition
				|| validGroupByCondition) {
			throw new RuntimeException("Column not found for aggregate request : " + params);
		}
	}

	static boolean checkTypeOfFunction(FunctionEnum functionName) {
		return (functionName.equals(FunctionEnum.XFREQCOUNT) || functionName.equals(FunctionEnum.XFREQPERC)
				|| functionName.equals(FunctionEnum.XPERC));
	}
	
	private JSONObject getIndicesAndOperations(String[] params, String[] parsedSelectCondition,
			String[] parsedGroupByCondition, FunctionEnum functionName) {
		JSONObject indicesAndOperations = new JSONObject();
		String[] matches = new String[] { MathConstants.GREATER_THAN_OR_EQUAL, MathConstants.LESS_THAN_OR_EQUAL,
				MathConstants.GREATER_THAN, MathConstants.LESS_THAN, MathConstants.NOT_EQUAL, MathConstants.SINGLE_EQUAL};
		
		String[] header = csvData.get(0);
		for (int j = 0; j < header.length; j++) {
			header[j] = FunctionDriver.cleanSingleQuoteStartAndEnd(header[j]);
			if (header[j].equals(parsedGroupByCondition[0].trim())) {
				indicesAndOperations.put(GROUPBYINDEX, j);
			}
			if (header[j].trim().equals(params[0].trim())) {
				indicesAndOperations.put(SELECTINDEX, j);
			}
			if (header[j].equals(parsedSelectCondition[0].trim())) {
				indicesAndOperations.put(CONDITIONINDEX, j);
			}
		}

		fetchingOperationsAndFunctionwiseIndexes(params, functionName, indicesAndOperations, matches);

		return indicesAndOperations;
	}

	/**
	 * @param params
	 * @param functionName
	 * @param indicesAndOperations
	 * @param matches
	 */
	private void fetchingOperationsAndFunctionwiseIndexes(String[] params, FunctionEnum functionName,
			JSONObject indicesAndOperations, String[] matches) {
		boolean checkCondition = true;
		boolean checkGroupBy = true;

		for (String match : matches) {
			int conditionIndex = -1;
			int groupByIndex = -1;
			if(checkTypeOfFunction(functionName)){
				conditionIndex = 2;
				groupByIndex = 3;
			}
			else{
				conditionIndex = 1;
				groupByIndex = 2;
			}
			
			if (checkCondition && params[conditionIndex].trim().contains(match)) {
				indicesAndOperations.put(CONDITIONINDEXOPERATOR, match);
				checkCondition = false;
			}
			if (checkGroupBy && params[groupByIndex].trim().contains(match)) {
				indicesAndOperations.put(GROUPBYINDEXOPERATOR, match);
				checkGroupBy = false;
			}
		}
	}

	/**
	 * Hierarchy is 
	 * 	1) Condition
	 * 	2) GroupBy
	 *  3) Select 
	 * 
	 * @param indicesAndOperations
	 * @param intermediateResult
	 * @param index
	 * @param parsedGroupByCondition
	 * @param param
	 */
	void getConditionBasedAggregation(JSONObject indicesAndOperations,
			HashMap<String, ArrayList<Object>> intermediateResult, int index, String[] parsedGroupByCondition,
			String param) {

		if (indicesAndOperations.containsKey(GROUPBYINDEX)) {
			getGroupByBasedAggregationOnly(indicesAndOperations, intermediateResult, index, parsedGroupByCondition,param);

		} else {
			getSelectionBasedAggregationOnly((int) indicesAndOperations.get(SELECTINDEX), intermediateResult, index,
					param,-1);
		}

	}

	void getGroupByBasedAggregationOnly(JSONObject indicesAndOperations,
			HashMap<String, ArrayList<Object>> intermediateResult, int index, String[] parsedGroupByCondition,
			String param) {
		if (indicesAndOperations.containsKey(GROUPBYINDEXOPERATOR)) {
			if (performOperation((String) indicesAndOperations.get(GROUPBYINDEXOPERATOR), index, indicesAndOperations,
					null, parsedGroupByCondition, GROUPBYCONDITION)) {
				getSelectionBasedAggregationOnly((int) indicesAndOperations.get(SELECTINDEX), intermediateResult, index,
						param,-1);
			}
		}
		else{
			getSelectionBasedAggregationOnly((int) indicesAndOperations.get(SELECTINDEX), intermediateResult, index,
					param,(int) indicesAndOperations.get(GROUPBYINDEX));
		}

	}

	void getSelectionBasedAggregationOnly(int selectIndex, HashMap<String, ArrayList<Object>> intermediateResult,
			int index, String param, int groupByIndex) {
		
		boolean groupByIndexCheck = (groupByIndex != -1);
		boolean notNullValue = groupByIndexCheck ? csvData.get(index)[groupByIndex] != null : false;
		
		// Only Select Column Present(Condition and Group By column Not Present)
		if ((groupByIndexCheck && intermediateResult.containsKey(csvData.get(index)[groupByIndex]))
				|| (intermediateResult.containsKey(param.trim()))) {
			String intermediateResultIndex = "";
			if (groupByIndexCheck && notNullValue) {
				intermediateResultIndex = csvData.get(index)[groupByIndex];
			} else {
				intermediateResultIndex = param.trim();
			}
			ArrayList<Object> intermediateArrayOfValues = intermediateResult
					.get(intermediateResultIndex);
			putIntoIntermediateArrayOfValues(selectIndex, index, intermediateArrayOfValues);
		} else {
			ArrayList<Object> intermediateArrayOfValues = new ArrayList<>();
			putIntoIntermediateArrayOfValues(selectIndex, index, intermediateArrayOfValues);
			
			String key = getKeyFromData(index, param, groupByIndex, groupByIndexCheck, notNullValue);
			
			if (null != key) {
				intermediateResult.put(key, intermediateArrayOfValues);
			}
		}
	}

	private String getKeyFromData(int index, String param, int groupByIndex, boolean groupByIndexCheck,
			boolean notNullValue) {
		if(groupByIndexCheck && notNullValue) {
			return csvData.get(index)[groupByIndex];
		}
		else if(groupByIndexCheck) {
			return null;
		}
		else {
			return param.trim();
		}
	}

	/**
	 * @param selectIndex
	 * @param index
	 * @param intermediateArrayOfValues
	 */
	private void putIntoIntermediateArrayOfValues(int selectIndex, int index,
			ArrayList<Object> intermediateArrayOfValues) {
		intermediateArrayOfValues.add(NumberUtils.isNumber(csvData.get(index)[selectIndex]) ? Double.parseDouble(csvData.get(index)[selectIndex]) : csvData.get(index)[selectIndex]);
	}

	boolean performOperation(String operation, int index, JSONObject indicesAndOperations, String[] parsedSelectCondition,
			String[] parsedGroupByCondition, String selectOrGroupByCondition) {
		
		String lhsValue = null;
		String rhsValue = null;

		switch (operation) {
		case MathConstants.SINGLE_EQUAL:
		case MathConstants.EQUAL:
		case MathConstants.NOT_EQUAL:
			switch (selectOrGroupByCondition) {
			case SELECTIONCONDITION:
				lhsValue = csvData.get(index)[(int) indicesAndOperations.get(CONDITIONINDEX)];
				rhsValue = parsedSelectCondition[parsedSelectCondition.length - 1];
				return performOperation(operation, index, indicesAndOperations, parsedSelectCondition, lhsValue,
						rhsValue);
			case GROUPBYCONDITION:
				lhsValue = csvData.get(index)[(int) indicesAndOperations.get(GROUPBYINDEX)];
				rhsValue = parsedGroupByCondition[parsedGroupByCondition.length - 1].trim();
				return performOperation(operation, index, indicesAndOperations, parsedSelectCondition, lhsValue,
						rhsValue);
			default :
			}
			break;
		default:
			try {
				switch (selectOrGroupByCondition) {
				case SELECTIONCONDITION:
					if (performComparisonOperation(operation,
							Double.parseDouble(csvData.get(index)[(int) indicesAndOperations.get(CONDITIONINDEX)]),
							Double.parseDouble(parsedSelectCondition[parsedSelectCondition.length - 1].trim()))) {
						return true;
					}
					break;
				case GROUPBYCONDITION:
					if (performComparisonOperation(operation,
							Double.parseDouble(csvData.get(index)[(int) indicesAndOperations.get(GROUPBYINDEX)]),
							Double.parseDouble(parsedGroupByCondition[parsedGroupByCondition.length - 1].trim()))) {
						return true;
					}
					break;
				default :	
				}
			} catch (Exception e) {
				return false;
			}
		}
		return false;
	}

	/**
	 * @param operation
	 * @param index
	 * @param indicesAndOperations
	 * @param parsedSelectCondition
	 * @param lhsValue
	 * @param rhsValue
	 * @return 
	 */
	private Boolean performOperation(String operation, int index, JSONObject indicesAndOperations,
			String[] parsedSelectCondition, String lhsValue, String rhsValue) {
		Map<String, Boolean> metaMap;
		try {
			// Null comparison
			metaMap = compareNullForEqualOrNotEqualCase(operation, lhsValue, rhsValue);
			if(metaMap.get(CONDITION_PERFORMED)) {
				return metaMap.get(RESULT);
			}
			//Numeric Comparison
			metaMap = compareNumericAndStringValuesForEqualOrNotEqualCase(operation, index, indicesAndOperations,
					parsedSelectCondition);
			if(metaMap.get(CONDITION_PERFORMED)) {
				return metaMap.get(RESULT);
			}
			
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	/**
	 * @param operation
	 * @param lhsValue
	 * @param rhsValue
	 * @return 
	 */
	private Map<String, Boolean> compareNullForEqualOrNotEqualCase(String operation, String lhsValue, String rhsValue) {
		Map<String, Boolean> metaMap = new HashMap<>();

		// Check if any of the value is null
		if (((operation.equals(MathConstants.EQUAL) || operation.equals(MathConstants.SINGLE_EQUAL))
				&& checkEqualOrNotEqualCondition("NULL", lhsValue, rhsValue))
				|| ((operation.equals(MathConstants.NOT_EQUAL))
						&& !checkEqualOrNotEqualCondition("NULL", lhsValue, rhsValue))) {
			metaMap.put(CONDITION_PERFORMED, true);
			metaMap.put(RESULT, true);
			return metaMap;
		}
		metaMap.put(CONDITION_PERFORMED, false);
		metaMap.put(RESULT, false);
		return metaMap;
	}
	
	boolean checkEqualOrNotEqualCondition(String type, String lhsValue, String rhsValue) {
		switch (type) {
		case "NULL":
			return (lhsValue == null || lhsValue.length() == 0) && (rhsValue == null || rhsValue.length() == 0);
		case "NORMAL":
			return StringUtils.equals(lhsValue, rhsValue);
		default:
			break;
		}
		return false;
	}
	

	/**
	 * @param operation
	 * @param index
	 * @param indicesAndOperations
	 * @param parsedSelectCondition
	 * @return 
	 */
	private Map<String, Boolean> compareNumericAndStringValuesForEqualOrNotEqualCase(String operation, int index,
			JSONObject indicesAndOperations, String[] parsedSelectCondition) {
		Map<String, Boolean> metaMap = new HashMap<>();
		boolean returnTrue = false;

		// Numeric comparison
		try {
			if (performComparisonOperation(operation,
					Double.parseDouble(csvData.get(index)[(int) indicesAndOperations.get(CONDITIONINDEX)]),
					Double.parseDouble(parsedSelectCondition[parsedSelectCondition.length - 1]))) {
				returnTrue = true;
			}
		} catch (Exception e) {
			// String comparison
			if (operation.equals(MathConstants.EQUAL) || operation.equals(MathConstants.SINGLE_EQUAL)) {
				if ((null != csvData.get(index)[(int) indicesAndOperations.get(CONDITIONINDEX)])
						&& (csvData.get(index)[(int) indicesAndOperations.get(CONDITIONINDEX)]
								.equals(parsedSelectCondition[parsedSelectCondition.length - 1].trim()))) {
					returnTrue = true;
				}
			} else if (!csvData.get(index)[(int) indicesAndOperations.get(CONDITIONINDEX)]
					.equals(parsedSelectCondition[parsedSelectCondition.length - 1].trim())) {
				returnTrue = true;
			}
		}
		if (returnTrue) {
			metaMap.put(CONDITION_PERFORMED, true);
			metaMap.put(RESULT, true);
			return metaMap;
		} else {
			metaMap.put(CONDITION_PERFORMED, false);
			metaMap.put(RESULT, false);
			return metaMap;
		}
	}
	
	static boolean performComparisonOperation(String operator,double lhs,double rhs){
		switch (operator) {
		case MathConstants.GREATER_THAN_OR_EQUAL:
			if (lhs >= rhs) {
				return true;
			}
			break;
		case MathConstants.GREATER_THAN:
			if (lhs > rhs) {
				return true;
			}
			break;
		case MathConstants.LESS_THAN_OR_EQUAL:
			if (lhs <= rhs) {
				return true;
			}
			break;
		case MathConstants.LESS_THAN:
			if (lhs < rhs) {
				return true;
			}
			break;
		case MathConstants.EQUAL:
		case MathConstants.SINGLE_EQUAL:
			if (lhs == rhs) {
				return true;
			}
			break;
		case MathConstants.NOT_EQUAL:
			if (lhs != rhs) {
				return true;
			}
			break;
		default :	
		}
		return false;
	}
}

package com.lti.mosaic.parser.exception;


/**
 * Licensed Information -- need to work on this 
 */

/**
 * Expception - In case syntax is not proper  
 * <p/>
 * 
 * @author Piyush
 * @since 1.0
 * @version 1.0
 */


public class SyntaxException extends ExpressionEvaluatorException {
	
	private static final long serialVersionUID = 1L;

	public SyntaxException(String message, Throwable throwable) {
		super(message, throwable);
	}

	public SyntaxException(String message) {
		super(message);
	}
}

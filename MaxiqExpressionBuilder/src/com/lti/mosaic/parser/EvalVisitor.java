package com.lti.mosaic.parser;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lti.mosaic.antlr4.parser.ExpressionBaseVisitor;
import com.lti.mosaic.antlr4.parser.ExpressionParser;
import com.lti.mosaic.antlr4.parser.ExpressionParser.ExprContext;
import com.lti.mosaic.function.def.Value;
import com.lti.mosaic.parser.driver.FunctionDriver;

 /**
  * 
  * @author Sarang Gandhi
  * @author Vivek Kasadwar
  * @version 0.1
  * 
  */

 /* 
  * The following class is entry point towards ANTLR4 parser
  *  with the usage of extending ExpressionBaseVisitor which is
  *  indeed the generic class to traverse all the grammar in the 
  *  Expression.g4 file.
  */
@SuppressWarnings("deprecation")
public class EvalVisitor extends ExpressionBaseVisitor<Value> {
	
	private static final Logger logger = LoggerFactory.getLogger(ExpressionBaseVisitor.class);
	
	// inputDataMaster will contain list of <uuid,data>
	private Map<String, ArrayList<String[]>> inputDataMaster = new HashMap<>();
	private List<String[]> data = new ArrayList<>();
	private Map<String, Object> otherData = new HashMap<>();
	private FunctionDriver functionDriver = new FunctionDriver(null,null,null,null,null);

	private static final String UNKNOWN_OPERATOR = "unknown operator: ";

	String uuid = "";
	
	public Map<String, ArrayList<String[]>> getInputDataMaster() {
		return inputDataMaster;
	}

	public void setInputDataMaster(Map<String, ArrayList<String[]>> inputDataMaster) {
		this.inputDataMaster = inputDataMaster;
	}

	public List<String[]> getData() {
		return data;
	}

	public void setData(List<String[]> data) {
		this.data = data;
	}

	public Map<String, Object> getOtherData() {
		return otherData;
	}

	public void setOtherData(Map<String, Object> otherData) {
		this.otherData = otherData;
	}

	public FunctionDriver getFunctionDriver() {
		return functionDriver;
	}

	public void setFunctionDriver(FunctionDriver functionDriver) {
		this.functionDriver = functionDriver;
	}

	// used to compare floating point numbers
	public boolean setCsvData(List<String[]> inputData, Map<String, Object> otherDataInput,
			Map<String, ArrayList<String[]>> inputDataMasterReq, Map<String, Value> aggregateFunctionCache, String expression) {
		this.data = inputData;
		this.otherData = otherDataInput;
		this.inputDataMaster = inputDataMasterReq;
		 //Execution of Function
		functionDriver = new FunctionDriver(inputDataMaster,data,otherData,aggregateFunctionCache,expression);
		return true;
	}



    // store variables (there's only one global scope!)
    private Map<String, Value> memory = new HashMap<>();

    //parent
	@Override
	public Value visitParse(ExpressionParser.ParseContext ctx) {
		return visit(ctx.block());
	}
    
    // assignment/id overrides
    @Override
    public Value visitAssignment(ExpressionParser.AssignmentContext ctx) {
        String id = ctx.ID().getText();
        Value value = this.visit(ctx.expr());
        memory.put(id, value);
        return value;
    }

    
    
    @Override
    public Value visitIdAtom(ExpressionParser.IdAtomContext ctx) {
        String id = ctx.getText();
		if (null == memory.get(id)) {
			throw new RuntimeException("no such variable: " + id);
		}
		logger.info("visitIdAtom >> variables : {}", memory);
        return memory.get(id);
    }

    // atom overrides
    @Override
    public Value visitStringAtom(ExpressionParser.StringAtomContext ctx) {
        String str = ctx.getText();
        // strip quotes
        str = str.substring(1, str.length() - 1).replace("\"\"", "\"");
        return new Value(str);
    }
    
    @Override
	public Value visitNumberAtom(ExpressionParser.NumberAtomContext ctx) {
    	return new Value(new BigDecimal(ctx.getText()));
	}

	@Override
	public Value visitArrayAtom(ExpressionParser.ArrayAtomContext ctx) {
		return visit(ctx.value_list());
	}
	
	@Override
	public Value visitValue_list(ExpressionParser.Value_listContext ctx) {
		Value list = null;
		if (null != ctx.array()) {
			list = visit(ctx.array());
		}
		return list;
	}

	@Override
	public Value visitArrayValues(ExpressionParser.ArrayValuesContext ctx) {
		Value[] result = new Value[ctx.arrayElement().size()];
		for (int i = 0; i < ctx.arrayElement().size(); i++) {
			result[i] = visit(ctx.arrayElement(i));
		}
		return new Value(result);
	}
	
	@Override
	public Value visitArrayElementTypes(ExpressionParser.ArrayElementTypesContext ctx) {
		return new Value(visit(ctx.atom()));
	}
	
	
    @Override
    public Value visitBooleanAtom(ExpressionParser.BooleanAtomContext ctx) {
        return new Value(Boolean.valueOf(ctx.getText()));
    }

    @Override
    public Value visitNilAtom(ExpressionParser.NilAtomContext ctx) {
        return null;
    }
    

    // expr overrides
	
    @Override
    public Value visitParExpr(ExpressionParser.ParExprContext ctx) {
        return this.visit(ctx.expr());
    }

	@Override
	public Value visitFunctionExpr(ExpressionParser.FunctionExprContext ctx) {
		return visit(ctx.functions());
	}

    @Override
    public Value visitPowExpr(ExpressionParser.PowExprContext ctx) {
        Value left = this.visit(ctx.expr(0));
        Value right = this.visit(ctx.expr(1));
        return EvalVisitorHelper.calculateBinaryOperations(left, right, ExpressionParser.POW, Locale.US);
    }

    @Override
	public Value visitUnaryMinusExpr(ExpressionParser.UnaryMinusExprContext ctx) {
		Value value = this.visit(ctx.expr());
		return new Value(BigDecimal.ZERO.subtract(value.asBigDecimal()));
 	}

    @Override
    public Value visitNotExpr(ExpressionParser.NotExprContext ctx) {
        Value value = this.visit(ctx.expr());
        return new Value(!value.asBoolean());
    }

	@Override
    public Value visitMultiplicationExpr( ExpressionParser.MultiplicationExprContext ctx) {

        Value left = this.visit(ctx.expr(0));
        Value right = this.visit(ctx.expr(1));
		if (left != null && right != null) {
			switch (ctx.op.getType()) {
			case ExpressionParser.MULT:
				return EvalVisitorHelper.calculateBinaryOperations(left, right, ExpressionParser.MULT, Locale.US);
			case ExpressionParser.DIV:
				return EvalVisitorHelper.calculateBinaryOperations(left, right, ExpressionParser.DIV, Locale.US);
			case ExpressionParser.MOD:
				return EvalVisitorHelper.calculateBinaryOperations(left, right, ExpressionParser.MOD, Locale.US);
			default:
				throw new RuntimeException(UNKNOWN_OPERATOR + ExpressionParser.tokenNames[ctx.op.getType()]);
			}
		}else{
			return null;
		}
    }

	@Override
	public Value visitAdditiveExpr(ExpressionParser.AdditiveExprContext ctx) {

		Value left = this.visit(ctx.expr(0));
		Value right = this.visit(ctx.expr(1));
		Boolean leftNull = Objects.nonNull(left);
		Boolean rightNull = Objects.nonNull(right);
		if (leftNull && rightNull) {
			switch (ctx.op.getType()) {
			case ExpressionParser.PLUS:
				return EvalVisitorHelper.calculateBinaryOperations(left, right, ExpressionParser.PLUS, Locale.US);
			case ExpressionParser.MINUS:
				return EvalVisitorHelper.calculateBinaryOperations(left, right, ExpressionParser.MINUS, Locale.US);
			default:
				throw new RuntimeException(UNKNOWN_OPERATOR + ExpressionParser.tokenNames[ctx.op.getType()]);
			}
		} else {
			return null;
		}
	}

	@Override
    public Value visitRelationalExpr(ExpressionParser.RelationalExprContext ctx) {

		Value left = this.visit(ctx.expr(0));
        Value right = this.visit(ctx.expr(1));
		
		if ((Objects.nonNull(left) && Objects.nonNull(right))
				&& (!(left.isBoolean() || left.isString() || left.isDate())
						&& !(right.isBoolean() || right.isString() || right.isDate()))) {
 			switch (ctx.op.getType()) {
			case ExpressionParser.LT:
				return EvalVisitorHelper.calculateBinaryOperations(left, right, ExpressionParser.LT, Locale.US);
			case ExpressionParser.LTEQ:
				return EvalVisitorHelper.calculateBinaryOperations(left, right, ExpressionParser.LTEQ, Locale.US);
			case ExpressionParser.GT:
				return EvalVisitorHelper.calculateBinaryOperations(left, right, ExpressionParser.GT, Locale.US);
			case ExpressionParser.GTEQ:
				return EvalVisitorHelper.calculateBinaryOperations(left, right, ExpressionParser.GTEQ, Locale.US);
			default:
				throw new RuntimeException(UNKNOWN_OPERATOR + ExpressionParser.tokenNames[ctx.op.getType()]);
			}
        }else{
			return new Value(false);
		}
    }

	@Override
	public Value visitEqualityExpr(ExpressionParser.EqualityExprContext ctx) {

		Value left = this.visit(ctx.expr(0));
		Value right = this.visit(ctx.expr(1));
		Boolean leftNull = Objects.nonNull(left);
		Boolean rightNull = Objects.nonNull(right);
		if (leftNull && rightNull) {
			switch (ctx.op.getType()) {
			case ExpressionParser.EQ:
				return EvalVisitorHelper.calculateBinaryOperations(left, right, ExpressionParser.EQ, Locale.US);
			case ExpressionParser.NEQ:
				return EvalVisitorHelper.calculateBinaryOperations(left, right, ExpressionParser.NEQ, Locale.US);
			default:
				throw new RuntimeException(UNKNOWN_OPERATOR + ExpressionParser.tokenNames[ctx.op.getType()]);
			}
		} else {
			return new Value(false);
		}
	}

    @Override
	public Value visitAndExpr(ExpressionParser.AndExprContext ctx) {
		Value left = this.visit(ctx.expr(0));
		Value right = this.visit(ctx.expr(1));
		Boolean leftNull = Objects.nonNull(left);
		Boolean leftRight = Objects.nonNull(right);
		if (leftNull && leftRight) {
			return new Value(Boolean.valueOf(left.toString()) && Boolean.valueOf(right.toString()));
		} else {
			return new Value(false);
		}
	}

    @Override
	public Value visitOrExpr(ExpressionParser.OrExprContext ctx) {
		Value left = this.visit(ctx.expr(0));
		Value right = this.visit(ctx.expr(1));
		Boolean leftNull = Objects.nonNull(left);
		Boolean rightNull = Objects.nonNull(right);
		if (leftNull && rightNull) {
			return new Value(Boolean.valueOf(left.toString()) || Boolean.valueOf(right.toString()));
		} else if (leftNull || rightNull) {
			if ((leftNull && Boolean.valueOf(left.toString()).equals(true))
					|| (rightNull && Boolean.valueOf(right.toString()).equals(true))) {
				return new Value(true);
			}
			else{
				return new Value(false);
			}
		} else {
			return new Value(false);
		}
	}

	//Basic router for expression

	@Override
	public Value visitIfStatExpr(ExpressionParser.IfStatExprContext ctx) {
		return visit(ctx.ifStat());
	}

	@Override
	public Value visitAssignmentExpr(ExpressionParser.AssignmentExprContext ctx) {
		return visit(ctx.assignment());
	}
	
	@Override
	public Value visitWhileStatExpr(ExpressionParser.WhileStatExprContext ctx) {
		return visit(ctx.whileStat());
	}

	@Override
	public Value visitStatementExpr(ExpressionParser.StatementExprContext ctx) {
		return visit(ctx.statement());
	}
    
    //Statement override
    @Override
    public Value visitStatement(ExpressionParser.StatementContext ctx) {
    	return this.visit(ctx.expr());
    }

    // if override
    @Override
    public Value visitIfStat(ExpressionParser.IfStatContext ctx) {

        List<ExpressionParser.ConditionBlockContext> conditions =  ctx.conditionBlock();

        boolean evaluatedBlock = false;
        Value evaluated =  null;
        for(ExpressionParser.ConditionBlockContext condition : conditions) {

             evaluated = this.visit(condition.expr());
            if(evaluated.asBoolean()) {
                evaluatedBlock = true;
                // evaluate this block whose expr==true
                 this.visit(condition.statBlock());
             }
        }

        if(!evaluatedBlock && ctx.statBlock() != null) {
            // evaluate the else-stat_block (if present == not null)
           this.visit(ctx.statBlock());
        }

        return null == evaluated ? Value.VOID :evaluated;
    }

    // while override
    @Override
	public Value visitWhileStat(ExpressionParser.WhileStatContext ctx) {

		Value value = this.visit(ctx.expr());

		while (value.asBoolean()) {

			// evaluate the code block
			this.visit(ctx.statBlock());

			// evaluate the expression
			value = this.visit(ctx.expr());
		}

		return value;
	}
    
    
    //Functions
    @Override
	public Value visitFunctionParam0(ExpressionParser.FunctionParam0Context ctx) {
		List<Value> params = new ArrayList<>();
		return functionDriver.executeFunction(ctx.FUNCTIONNAME_PARAM0().getText(), params);
	}
    
	@Override
	public Value visitFunctionParam1(ExpressionParser.FunctionParam1Context ctx) {
		// Check if any parameter is null
		Boolean checkFirstArgument = null != ctx.arguments1().param1;

		List<Value> params = new ArrayList<>();
		params.add(checkFirstArgument ? visit(ctx.arguments1().expr()) : null);

		return functionDriver.executeFunction(ctx.FUNCTIONNAME_PARAM1().getText(), params);
	}

	@Override
	public Value visitFunctionParam2(ExpressionParser.FunctionParam2Context ctx) {
 		// Check if any parameter is null
 		Boolean checkFirstArgument = null != ctx.arguments2().param1;
		Boolean checkSecondArgument = null != ctx.arguments2().param2;

		List<Value> params = new ArrayList<>();
		List<ExprContext> exprList = ctx.arguments2().expr();
		for (ExprContext exprContext : exprList) {
			params.add(visit(exprContext));
		}
		
		if(!checkFirstArgument){
			params.add(0,null);
		}
		if(!checkSecondArgument){
			params.add(1,null);
		} 

		return functionDriver.executeFunction(ctx.FUNCTIONNAME_PARAM2().getText(), params);

	}

	@Override
	public Value visitFunctionParam3(ExpressionParser.FunctionParam3Context ctx) {
		
		Boolean checkFirstArgument = null != ctx.arguments3().param1;
		Boolean checkSecondArgument = null != ctx.arguments3().param2;
		Boolean checkThirdArgument = null != ctx.arguments3().param3;

		List<Value> params = new ArrayList<>();
		List<ExprContext> exprList = ctx.arguments3().expr();
		for (ExprContext exprContext : exprList) {
			params.add(visit(exprContext));
		}
		
		if(!checkFirstArgument){
			params.add(0,null);
		}
		if(!checkSecondArgument){
			params.add(1,null);
		} 
		if(!checkThirdArgument){
			params.add(2,null);
		}
		
		return functionDriver.executeFunction(ctx.FUNCTIONNAME_PARAM3().getText(), params);
	}

	@Override
	public Value visitFunctionParam4(ExpressionParser.FunctionParam4Context ctx) {
		
		Boolean checkFirstArgument = null != ctx.arguments4().param1;
		Boolean checkSecondArgument = null != ctx.arguments4().param2;
		Boolean checkThirdArgument = null != ctx.arguments4().param3;
		Boolean checkForthArgument = null != ctx.arguments4().param4;
		
		List<Value> params = new ArrayList<>();
		List<ExprContext> exprList = ctx.arguments4().expr();
		for (ExprContext exprContext : exprList) {
			params.add(visit(exprContext));
		}
		
		if(!checkFirstArgument){
			params.add(0,null);
		}
		if(!checkSecondArgument){
			params.add(1,null);
		} 
		if(!checkThirdArgument){
			params.add(2,null);
		}
		if(!checkForthArgument){
			params.add(3,null);
		}
		
		return functionDriver.executeFunction(ctx.FUNCTIONNAME_PARAM4().getText(), params);
	}
	
	
	@Override
	public Value visitFunctionParamN(ExpressionParser.FunctionParamNContext ctx) {
		List<ExprContext> exprList = ctx.argumentsN().expr();
				
		List<Value> params = new ArrayList<>(); 
		for (ExprContext exprContext : exprList) {
			params.add(visit(exprContext));
		}
		
		return functionDriver.executeFunction(ctx.FUNCTIONNAME_PARAM_N().getText(), params);
	}

}

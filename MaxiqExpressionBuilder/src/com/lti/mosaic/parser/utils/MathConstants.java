package com.lti.mosaic.parser.utils;

public class MathConstants {
	
	private MathConstants() {
	}
	
	protected static final String[] OPERATORS = { "!=", "==", ">=", "<=", ">",
			"<", "||", "&&", "*", "/", "+", "-", "^" };
	public static final String DEVIDE = "/";
	public static final String POWER = "^";
	public static final String MULTIPLY = "*";
	public static final String ADDITION = "+";
	public static final String SUBTRACT = "-";
	public static final String LOGICAL_OR = "||";
	public static final String LOGICAL_AND = "&&";
	public static final String EQUAL = "==";
	public static final String SINGLE_EQUAL = "=";
	public static final String GREATER_THAN = ">" ;
	public static final String LESS_THAN = "<" ;
	public static final String GREATER_THAN_OR_EQUAL = ">=" ;
	public static final String LESS_THAN_OR_EQUAL = "<=" ;
	public static final String NOT_EQUAL = "!=" ;
}

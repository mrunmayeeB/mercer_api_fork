package com.lti.mosaic.api.worker.capacitymanager;

import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Timer;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import in.lnt.utility.constants.LoggerConstants;

/**
 * @author rushikesh
 *
 */
public class CapacityManagerThread implements Runnable {

  private static final Logger logger = LoggerFactory.getLogger(CapacityManagerThread.class);

  private static Boolean loadIsBelowThreshold = true;

  /**
   * @return the loadIsBelowThreshold
   */
  public static Boolean getLoadIsBelowThreshold() {
    return loadIsBelowThreshold;
  }

  private static void getResourceStats() {
    
    logger.debug(LoggerConstants.LOG_MAXIQAPI, ">> getResourceStats");

    OperatingSystemMXBean osMBean = ManagementFactory.getOperatingSystemMXBean();

    for (Method method : osMBean.getClass().getDeclaredMethods()) {
      method.setAccessible(true);
      String methodName = method.getName();

      if (StringUtils.equalsIgnoreCase(methodName, "getSystemCpuLoad")
          && Modifier.isPublic(method.getModifiers())) {

        Object value;
        try {
          value = method.invoke(osMBean);
        } catch (Exception e) {
         logger.error(" exception occured in getResourceStats() method {}", e.getMessage());
          value = e;
        }
        // here 0.75 indicates 75 percent cpu usage
        if ((Double) value > 0.75) {
          logger.debug(LoggerConstants.LOG_MAXIQAPI, ">> getResourceStats threshold reached");
          loadIsBelowThreshold = false;
        } else {
          loadIsBelowThreshold = true;
        }
      }
    }
    

  }

  /* (non-Javadoc)
   * @see java.lang.Runnable#run()
   */
  @Override
  public void run() {
    
    logger.debug(LoggerConstants.LOG_MAXIQAPI, ">> run");
    boolean doContinue = true;

    while (doContinue) {
      try {
        getResourceStats();
        Thread.sleep(1000);
      } catch (Exception e) {
        logger.error(LoggerConstants.LOG_MAXIQAPI, ">> run {}", e.getMessage());
      }
    }
    logger.error("Error in consumer thread - all done {}", LoggerConstants.LOG_MAXIQAPI);
  }
  
}
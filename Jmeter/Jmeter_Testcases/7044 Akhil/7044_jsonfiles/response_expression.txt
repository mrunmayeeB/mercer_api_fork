{
    "entities": [
        {
            "sections": [
                {
                    "sectionCode": "incumbent_data",
                    "data": [
                        {
                            "BIRTH_YEAR": "true"
                        },
                        {
                            "BIRTH_YEAR": "false"
                        }
                    ]
                },
                {
                    "sectionCode": "company_data",
                    "data": [
                        {
                            "POS_CODE": "",
                            "EXCLUDE_FLAG": "Y"
                        }
                    ]
                }
            ],
            "contextData": {
                "companyId": "12345",
                "campaignId": "11111",
                "grpCode": "1435",
                "cpyCode": "4444",
                "industry": {
                    "superSector": "CG",
                    "subSector": "101",
                    "sector": "100"
                },
                "orgSize": 123,
                "ctryCode": "PL"
            }
        }
    ]
}